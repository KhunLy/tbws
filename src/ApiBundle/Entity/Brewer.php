<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Brewer
 *
 * @ORM\Table(name="brewer")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\BrewerRepository")
 */
class Brewer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"getBrewer"})
     *
     *
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", length=100, type="string", unique=true, nullable=false)
     * @Serializer\Groups({"getBrewer"})
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="address", length=255, type="string", unique=false, nullable=false)
     * @Serializer\Groups({"getBrewer"})
     */
    private $address;

    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Country")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Groups({"getBrewer"})
     */
    private $country;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Brewer
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Brewer
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     * @return Brewer
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }


}

