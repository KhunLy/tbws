<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Bier
 *
 * @ORM\Table(name="bier")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\BierRepository")
 */
class Bier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", length=255, type="string",nullable=false, unique=false)
     */
    private $name;

    /**
     * @var float
     * @ORM\Column(name="degree", type="float",nullable=true, unique=false)
     */
    private $degree;

    /**
     * @var integer
     * @ORM\Column(name="capacity", type="integer",nullable=true, unique=false)
     */
    private $capacity;

    /**
     * @var string
     * @ORM\Column(name="picture", type="string", length=255,nullable=true, unique=true)
     */
    private $picture;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;

    /**
     * @var Brewer
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Brewer")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brewer;

    /**
     * @var UploadedFile
     * @Assert\File(maxSize="2M")
     */
    private $file;

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @return Bier
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Bier
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * @param float $degree
     * @return Bier
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;
        return $this;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     * @return Bier
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Bier
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Bier
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Brewer
     */
    public function getBrewer()
    {
        return $this->brewer;
    }

    /**
     * @param Brewer $brewer
     * @return Bier
     */
    public function setBrewer($brewer)
    {
        $this->brewer = $brewer;
        return $this;
    }
}

