<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\CountryRepository")
 */
class Country
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"getBrewer"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", length=100, type="string", unique=true, nullable=false)
     * @Serializer\Groups({"getBrewer"})
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="isocode", length=2, type="string", unique=true, nullable=false)
     */
    private $isocode;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsocode()
    {
        return $this->isocode;
    }

    /**
     * @param string $isocode
     * @return Country
     */
    public function setIsocode($isocode)
    {
        $this->isocode = $isocode;
        return $this;
    }


}

