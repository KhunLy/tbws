<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MessageController extends FOSRestController
{
    /**
     * @Rest\Get(path="/message")
     * @Rest\View(serializerGroups={"getMessage"})
     */
    public function getAction()
    {
        return $this->getDoctrine()
            ->getRepository(Message::class)
            ->findAll();
    }

    /**
     * @param Request $request
     * @Rest\Post(path="/message")
     * @Rest\View()
     * @return \Symfony\Component\Form\FormInterface
     */
    public function postAction(Request $request)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $message->setDate(new \DateTime());
            $em->persist($message);
            $em->flush();
        }
        return $form;
    }
}
