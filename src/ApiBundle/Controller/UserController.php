<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

class UserController extends FOSRestController
{
    /**
     * @Rest\Get(path="/user")
     * @Rest\View(serializerGroups={"getUser"})
     */
    public function getAction()
    {
        return $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();
    }

}
