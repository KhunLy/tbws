<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Brewer;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

class CountryController extends FOSRestController
{
    /**
     * @Rest\Get(path="/country/used")
     * @Rest\View()
     */
    public function getUsedCountryAction()
    {
        $repo = $this->getDoctrine()->getRepository(Brewer::class);
        $countries = array_map(function($brewer){
            /**@var $brewer Brewer */
            return $brewer->getCountry();
        },$repo->getCountry());
        //dump($countries);die;
        return $countries;
    }
}
