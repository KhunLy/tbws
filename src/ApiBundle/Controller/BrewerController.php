<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Brewer;
use ApiBundle\Entity\Country;
use ApiBundle\Form\BrewerType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class BrewerController extends FOSRestController
{
    /**
     * @Rest\Get(path="/brewer")
     * @Rest\View(serializerGroups={"getBrewer"})
     */
    public function getAction()
    {
        return $this->getDoctrine()
            ->getRepository(Brewer::class)
            ->findAll();
    }

    /**
     * @Rest\Put(path="/brewer/{brewerID}")
     * @Rest\View()
     */
    public function putAction(Request $request, Brewer $brewerID)
    {
        //dump($brewerID);die;
        $form = $this->createForm(BrewerType::class, $brewerID, ["method" => "put"]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $brewerID;
        }
        return $form;
    }

    /**
     * @param Brewer $brewerID
     * @Rest\Get(path="/brewer/{brewerID}")
     * @Rest\View()
     * @return Brewer
     */
    public function getByIdAction(Brewer $brewerID)
    {
        return $brewerID;
    }

    /**
     * @Rest\Post(path="/brewer")
     * @Rest\View()
     */
    public function postAction(Request $request)
    {
        $brewer = new Brewer();
        $form = $this->createForm(BrewerType::class, $brewer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($brewer);
            $em->flush();
            return $brewer;
        }
        return $form;
    }

    /**
     * @Rest\Get(path="/brewer/byCountry/{countryID}")
     * @Rest\View()
     */
    public function getByCountryAction(Country $countryID)
    {
        return $this->getDoctrine()->getRepository(Brewer::class)
            ->findBy(["country" => $countryID]);
    }

    /**
     * @Rest\Delete(path="/brewer/{brewerID}")
     * @Rest\View()
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteAction($brewerID)
    {
        $em = $this->getDoctrine()->getManager();
        $brewer = $em
            ->getRepository(Brewer::class)
            ->find($brewerID);


        $em->remove($brewer);
        $em->flush();
        return $brewer;

    }


}
