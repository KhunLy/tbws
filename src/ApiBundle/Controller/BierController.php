<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Bier;
use ApiBundle\Form\BierType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;


class BierController extends FOSRestController
{
    /**
     * @param Request $request
     * @Rest\Post(path="/beer")
     * @Rest\View()
     * @return Bier|\Symfony\Component\Form\FormInterface
     */
    public function postAction(Request $request)
    {
         $beer = new Bier();
         $form = $this->createForm(BierType::class, $beer);
         $form->handleRequest($request);
         if($form->isSubmitted() && $form->isValid()){
             $file = $beer->getFile();
             $uniquePath = md5(uniqid()).$file->getClientOriginalName();
             $file->move(
                 $this->getParameter("beer_directory"),
                 $uniquePath
             );
             $beer->setPicture($uniquePath);
             $em = $this->getDoctrine()->getManager();
             $em->persist($beer);
             $em->flush();
             return $beer;
         }
         return $form;


    }
}
