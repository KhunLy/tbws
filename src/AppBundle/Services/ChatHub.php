<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 02-07-18
 * Time: 11:18
 */

namespace AppBundle\Services;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ChatHub implements MessageComponentInterface
{
    private $container;
    /**
     * @var ConnectionInterface[]
     */
    private $clients;

    public function __construct(ContainerInterface $cont)
    {
        $this->container = $cont;
        $this->clients = [];
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    function onOpen(ConnectionInterface $conn)
    {
        echo "un utilisateur s'est connecté\n";
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn)
    {
        foreach ($this->clients as $key => $client){
            if ($conn == $client){
                unset($this->clients[$key]);
            }
        }
        $this->refreshUsers();
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        // TODO: Implement onError() method.
        echo $e->getMessage() . "\n";
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $from The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    function onMessage(ConnectionInterface $from, $msg)
    {
        $data = json_decode($msg);
        $dataPath = $data->path;
        $datatoken = $data->token; //token d'identification
        $token = new JWTUserToken();
        $token->setRawToken($datatoken);
        switch ($dataPath){
            case "/login":
                //traitement
                $username = $this->getUser($token);
                $this->clients[$username] = $from;
                $this->refreshUsers();
                break;
            case "/history":
                $dest = $data->dest;
                $username = $this->getUser($token);
                $this->refreshMessages($username, $dest);
                break;
            case "/newMessage":
                $em = $this->getDoctrine()->getManager();
                $uRepo = $this->getDoctrine()
                    ->getRepository(User::class);
                $dataMessage = $data->message;
                $dataDest = $data->dest;
                $username = $this->getUser($token);
                $author = $uRepo->findOneBy([
                    "username" => $username
                ]);
                $receiver = $uRepo->findOneBy([
                    "username" => $dataDest
                ]);
                /**
                 * @var $author User
                 * @var $receiver User
                 */
                $message = new Message();
                $message->setContent($dataMessage)
                    ->setReceiver($receiver)
                    ->setAuthor($author)
                    ->setDate(new \DateTime())
                ;
                $em->persist($message);
                $em->flush();

                //maj from messages history
                $this->refreshMessages($username, $dataDest);
                //notification destinataire
                $messageRepo = $this->getDoctrine()
                    ->getRepository(Message::class);
                $messages = $messageRepo->getHistory($dataDest, $username);
                $twig = $this->container->get('templating');
                $view = $twig->render("Chat/messageHistory.html.twig", [
                    "messages" => $messages,
                    "username" => $dataDest,
                ]);
                $this->clients[$dataDest]->send(json_encode([
                    "action" => "message_received",
                    "from" => $username,
                    "view" => $view
                ]));
                break;
        }


    }
    private function refreshUsers(){
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()
            ->getRepository(User::class);
        $twig = $this->container->get("templating");
        $users = [];
//        foreach (array_keys($this->clients) as $name){
//            $user = $repo->findOneBy(["username" => $name]);
//            $em->refresh($user);
//            $users[] = $user;
//        }
        $view = $twig->render("Chat/listUsers.html.twig", [
            "users" => array_map(function($username)use($repo, $em){
                $user = $repo->findOneBy(["username" => $username]);
                $em->refresh($user);
                return $user;
            }, array_keys($this->clients))
        ]);
        //envoi d'une vue partielle
        $response = json_encode([
            "action" => "refresh_users",
            "view" => $view
        ]);
        foreach ($this->clients as $client){
            $client->send($response);
        }
    }

    private function refreshMessages($username, $dest){
        $messageRepo = $this->getDoctrine()
            ->getRepository(Message::class);
        $messages = $messageRepo->getHistory($username, $dest);
        $twig = $this->container->get('templating');
        $view = $twig->render("Chat/messageHistory.html.twig", [
            "messages" => $messages,
            "username" => $username,
        ]);
        $this->clients[$username]->send(json_encode([
            "action" => "refresh_messages",
            "view" => $view
        ]));
    }

    private function getDoctrine(){
        return $this->container->get("doctrine");
    }

    private function getUser($token){
        $tokenManager =
            $this->container
                ->get("lexik_jwt_authentication.jwt_manager");
        $payload = $tokenManager->decode($token);
        return $payload["username"];
    }
}