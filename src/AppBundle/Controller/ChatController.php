<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ChatController extends Controller
{
    /**
     * @Route(name="chat_message", path="/chat")
     * @Security("has_role('ROLE_USER')")
     */
    public function messageAction()
    {
        $tm = $this->get("lexik_jwt_authentication.jwt_manager");
        $token = $tm->create($this->getUser());
        dump($token);
        return $this->render("Chat/message.html.twig", [
            "token" => $token
        ]);
    }

    /**
     * @Route(name="login", path="/login")
     */
    public function loginAction(AuthenticationUtils $utils)
    {
        $username = $utils->getLastUsername();
        $error = $utils->getLastAuthenticationError();
        return $this->render(":Chat:login.html.twig", [
            "username" => $username,
            "error" => $error
        ]);

    }

    /**
     * @Route(name="register", path="/register")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $filename
                = md5(uniqid()) . $user->getFile()->getClientOriginalName();
            $user->getFile()->move(
                $this->getParameter("avatar_directory"),
                $filename
            );
            $user->setAvatar($filename);
            $user->setRole(["ROLE_USER"]);
            $pwd = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($pwd);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute("login");
        }
        return $this->render(":Chat:register.html.twig", [
           "form" => $form->createView()
        ]);
    }

}
