<?php
/**
 * Created by PhpStorm.
 * User: Kly
 * Date: 02-07-18
 * Time: 10:55
 */

namespace AppBundle\Command;


use AppBundle\Services\ChatHub;
use Ratchet\App;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName("ws:run");
        $this->setDescription("démarre de serveur websocket");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
         $output->write("Tentative de connection au chat");
//         $server = IoServer::factory(
//             new HttpServer(
//                 new WsServer(
//                     new ChatHub($this->getContainer())
//                 )
//             ), 8080
//         );
//         $server->run();

        $server = new App("localhost", 8080);
        $server->route("/", new ChatHub($this->getContainer()));
        $server->run();

    }
}